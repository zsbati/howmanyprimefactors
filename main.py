def primes_in(n):
    #
    # Write your code here.
    
    def is_prime(n):
        
        if n==1:
            return False
        
        if n==2:
            return True
        
        if n != 2 and n%2 == 0:
            return False    
        
        for j in range(int(n**0.5)+2):
          if (n%(j+2) == 0 and j+2 < n):
            return False

        return True      
            
    
    prime_count = 0
    
    if n == 1:
        return 0
        
    if n==2:
        return 1    
    
    if n == 3:
      return 1

    if n == 5:
      return 1  

    if n>=6:
     
      increment = 0;
      for i in range(int(n**0.5)+3):
        
        if is_prime(i+2):
          
          if n%(i+2) == 0:
          
            increment += 1
      if increment == 0:
        increment = 1
      prime_count =+ increment  
        
            
    
    return prime_count
    #
            
print(primes_in(5000))            
